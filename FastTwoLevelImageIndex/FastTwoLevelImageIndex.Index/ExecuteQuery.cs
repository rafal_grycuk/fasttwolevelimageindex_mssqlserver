using FastTwoLevelImageIndex.Index.OpenSURF;
using FastTwoLevelImageIndex.Index.ViewModels;
using Microsoft.SqlServer.Server;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
using System.Linq;

namespace FastTwoLevelImageIndex.Index
{
    public partial class UserDefinedFunctions
    {
        [SqlFunction(DataAccess = DataAccessKind.Read, FillRowMethodName = "FillRow",
            TableDefinition = "Id int, TwoFactorDescriptor nvarchar(max), ImageName nvarchar(max), Extension nvarchar(max), Tag nvarchar(max), ImageBinaryContent varbinary(max), Distance float, ImageDatasetName nvarchar(max)")]
        public static IEnumerable ExecuteQuery([SqlFacet(MaxSize = -1)] SqlBinary image, int threshold, string imageDatasetName = null)
        {
            if (!image.IsNull && !string.IsNullOrWhiteSpace(imageDatasetName))
            {
                var queryImage = new IndexedImage()
                {
                    TwoFactorDescriptor = new TwoFactorDescriptor { Index = new Dictionary<int, Dictionary<int, List<IPoint>>>() }
                };
                Stream memoryStream = new MemoryStream(image.Value);
                IntegralImage integralImageQuery;
                using (var img = new Bitmap(Image.FromStream(memoryStream)))
                {
                    integralImageQuery = IntegralImage.FromImage(img);
                }
                var ipts = FastHessian.getIpoints(0.0002f, 5, 2, integralImageQuery);
                SurfDescriptor.DecribeInterestPoints(ipts, false, false, integralImageQuery);
                ipts.ForEach(keypoint =>
                {
                    var hashes = queryImage.TwoFactorDescriptor.CreateHash(keypoint.descriptor);
                    queryImage.TwoFactorDescriptor.AddKeyPoint(hashes, keypoint);
                });
                var indexedImages = new List<IndexedImage>();
                using (var connection = new SqlConnection("context connection=true"))
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    var cmdStr =
                        "SELECT Id,TwoFactorDescriptor.ToString() as TwoFactorDescriptor,ImageName, Extension, Tag, ImageBinaryContent, ImageDatasetName" +
                        " FROM dbo.Images ";
                    if (!string.IsNullOrWhiteSpace(imageDatasetName))
                    {
                        cmdStr += " WHERE  ImageDatasetName like @ImageDatasetName";
                    }

                    var command = new SqlCommand(cmdStr, connection);
                    command.Parameters.Add(new SqlParameter("@imageDatasetName", SqlDbType.VarChar));
                    command.Parameters["@imageDatasetName"].Value = imageDatasetName;
                    var dataReader = command.ExecuteReader();
                    if (!dataReader.HasRows)
                    {
                        return null;
                    }

                    while (dataReader.Read())
                    {
                        var indexedImage = new IndexedImage()
                        {
                            TwoFactorDescriptor = dataReader["TwoFactorDescriptor"] is string
                                ? dataReader["TwoFactorDescriptor"].ToString()
                                : null,
                            Id = dataReader.GetInt32(0),
                            ImageName = dataReader.GetString(2),
                            Extension = dataReader.GetString(3),
                            Tag = dataReader.GetString(4),
                            ImageBinaryContent = dataReader["ImageBinaryContent"] is byte[]
                                ? dataReader["ImageBinaryContent"] as byte[]
                                : null,
                            ImageDatasetName = dataReader.GetString(6)
                        };
                        indexedImages.Add(indexedImage);
                    }
                    var queryResults = new List<QueryResult>();
                    foreach (var indexedImage in indexedImages)
                    {
                        var queryResult = CompareImages(queryImage, indexedImage);
                        queryResults.Add(queryResult);
                    }

                    queryResults = queryResults.OrderByDescending(x => x.Distance.Item1).ThenBy(x => x.Distance.Item2).ToList();
                    return queryResults.Where(r => r.Distance.Item2 >= threshold).OrderBy(r => r.Distance);
                }
            }
            throw new Exception("Invalid Parameters.");
        }

        public static void FillRow(object queryResult, out SqlInt32 id, [SqlFacet(MaxSize = -1)] out SqlString twoFactorDescriptor,
            out SqlString imageName, out SqlString extension, out SqlString tag,
            [SqlFacet(MaxSize = -1)] out SqlBinary imageBinaryContent, out SqlDouble distance, out SqlString imageDatasetName)
        {
            QueryResult result = (QueryResult)queryResult;
            id = result.Id;
            imageName = result.ImageName;
            twoFactorDescriptor = new SqlString(result.TwoFactorDescriptor.ToString());
            extension = result.Extension;
            distance = result.Distance.Item2;
            tag = result.Tag;
            imageBinaryContent = result.ImageBinaryContent;
            imageDatasetName = result.ImageDatasetName;
        }

        /// <summary>
        /// Method comparing images and returns I_2 image with simialiry measure
        /// </summary>
        /// <param name="I_1">Query image</param>
        /// <param name="I_2">Given images</param>
        /// <returns></returns>
        public static QueryResult CompareImages(IndexedImage I_1, IndexedImage I_2)
        {
            double sumDistance = 0.0;
            int hashesMatched = 0;
            foreach (var I_1_H1 in I_1.TwoFactorDescriptor.Index)
            {
                if (I_2.TwoFactorDescriptor.Index.ContainsKey(I_1_H1.Key))
                {
                    foreach (var I_1_H2 in I_1_H1.Value) 
                    {
                        foreach (var I_2_H2 in I_2.TwoFactorDescriptor.Index[I_1_H1.Key])
                        {
                            if (I_1_H2.Key == I_2_H2.Key || (I_1_H2.Key ^ I_2_H2.Key) < 16)
                            {
                                ++hashesMatched;
                                var desciptorsSimilarityList = new List<Tuple<IPoint, IPoint, double>>();
                                foreach (var I_1_Desc in I_1_H2.Value)
                                {
                                    foreach (var I_2_Desc in I_2_H2.Value)
                                    {
                                        double descriptorsSimilarity = CompareDescriptors(I_1_Desc.descriptor, I_2_Desc.descriptor);
                                        var tuple = new Tuple<IPoint, IPoint, double>(I_1_Desc, I_2_Desc, descriptorsSimilarity);
                                        desciptorsSimilarityList.Add(tuple);
                                    }
                                }
                                desciptorsSimilarityList = desciptorsSimilarityList.OrderBy(x => x.Item3).ToList();
                                sumDistance += desciptorsSimilarityList.Sum(x => x.Item3);
                            }
                        }
                    }
                }
            }

            var result = new QueryResult()
            {
                TwoFactorDescriptor = I_2.TwoFactorDescriptor,
                Extension = I_2.Extension,
                Tag = I_2.Tag,
                ImageBinaryContent = I_2.ImageBinaryContent,
                Id = I_2.Id,
                ImageName = I_2.ImageName,
                ImageDatasetName = I_2.ImageDatasetName,
                Distance = new Tuple<int, double>(hashesMatched, sumDistance)
            };
            return result;
        }

        public static double CompareDescriptors(float[] a, float[] b)
        {
            double sum = 0;
            if (a.Length == b.Length)
            {
                for (int i = 0; i < a.Length; i++)
                {
                    sum += Math.Pow(a[i] - b[i], 2);
                }
                sum = Math.Sqrt(sum);
            }
            return sum;
        }

    }
}
