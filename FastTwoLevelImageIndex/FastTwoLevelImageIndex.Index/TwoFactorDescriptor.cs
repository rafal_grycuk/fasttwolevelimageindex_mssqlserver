using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
using FastTwoLevelImageIndex.Index.OpenSURF;
using Microsoft.SqlServer.Server;
using Newtonsoft.Json;
using System.Linq;

namespace FastTwoLevelImageIndex.Index
{
    [Serializable]
    [SqlUserDefinedType(Format.UserDefined, IsByteOrdered = true, MaxByteSize = -1)]
    public class TwoFactorDescriptor : INullable, IBinarySerialize
    {
        private bool _null;
        public Dictionary<int, Dictionary<int, List<IPoint>>> Index { get;  set; }

        public void AddKeyPoint(Tuple<int, int> hashes, IPoint keyPoint)
        {
            if (!Index.ContainsKey(hashes.Item1))
            {
                Index.Add(hashes.Item1, new Dictionary<int, List<IPoint>>());
            }

            if (!Index[hashes.Item1].ContainsKey(hashes.Item2))
            {
                Index[hashes.Item1].Add(hashes.Item2, new List<IPoint>());
            }

            Index[hashes.Item1][hashes.Item2].Add(keyPoint);
        }
        public Tuple<int, int> CreateHash(float[] descriptor)
        {
            var a = descriptor;
            var b = new double[16];
            var C = new double[16];
            var t = new double[16];
            var I_1 = 0;
            var I_2 = 0;
            var D_1 = new int[16];
            var D_2 = new int[16];
            const float closestTo4 = 3.99f;
            for (var j = 0; j < 16; j++)
            {
                for (var k = 0; k < 3; k++)
                {
                    //  b[j] += Math.Sqrt(Math.Abs(a[4 * j + k]));
                    b[j] += a[16 * k + j] * a[16 * k + j];
                }
            }
            var b_max = b.Max();
            for (var j = 0; j < 16; j++)
            {
                C[j] = closestTo4 * b[j] / b_max;
            }

            for (var j = 0; j < 16; j++)
            {
                D_1[j] = (int)Math.Floor(C[j]);
            }

            for (var j = 0; j < 16; j++)
            {
                I_1 += D_1[j] * (int)Math.Pow(4, j);
            }

            for (var j = 0; j < 16; j++)
            {
                t[j] = C[j] - D_1[j];
            }

            for (var j = 0; j < 16; j++)
            {
                if (D_1[j] > 0 && t[j] < 0.3)
                {
                    D_2[j] = D_1[j] - 1; //(int)Math.Floor(C[j] - 1);
                }
                else if (t[j] > 0.6 && D_1[j] < 1)
                {
                    D_2[j] = D_1[j] + 1; //(int)Math.Floor(C[j] + 1);
                }
                else
                {
                    D_2[j] = D_1[j]; //(int)Math.Floor(C[j]);
                }
            }

            for (var j = 0; j < 16; j++)
            {
                I_2 += D_2[j] * (int)Math.Pow(4, j);
            }

            return new Tuple<int, int>(I_1, I_2);
        }

        #region Predefined Members
        [return: SqlFacet(MaxSize = -1)]
        public override string ToString()
        {
            string str = JsonConvert.SerializeObject(Index, Formatting.Indented);
            return str;
        }
        public static implicit operator TwoFactorDescriptor(string indexJson)
        {
            return new TwoFactorDescriptor()
            {
                Index = JsonConvert.DeserializeObject<Dictionary<int, Dictionary<int, List<IPoint>>>>(indexJson)
            };
        }
        public bool IsNull => _null;

        public static TwoFactorDescriptor Null
        {
            get
            {
                var h = new TwoFactorDescriptor { _null = true };
                return h;
            }
        }

        public static TwoFactorDescriptor Parse(SqlString pathToImage)
        {
            if (pathToImage.IsNull || string.IsNullOrWhiteSpace(pathToImage.Value))
                return Null;
            TwoFactorDescriptor twoFactorDescriptor = new TwoFactorDescriptor { Index = new Dictionary<int, Dictionary<int, List<IPoint>>>() };
            IntegralImage iimg;
            using (var img = new Bitmap(pathToImage.Value))
            {
                iimg = IntegralImage.FromImage(img);
            }
            // Extract the interest points
            var ipts = FastHessian.getIpoints(0.0002f, 5, 2, iimg);
            // Describe the interest points
            SurfDescriptor.DecribeInterestPoints(ipts, false, false, iimg);
            // foreach keypoint create a hash and add it to dictionary.
            ipts.ForEach(keypoint =>
            {
                var hashes = twoFactorDescriptor.CreateHash(keypoint.descriptor);
                twoFactorDescriptor.AddKeyPoint(hashes, keypoint);
            });
            return twoFactorDescriptor;
        }

        public void Read(BinaryReader binaryReader)
        {
            _null = binaryReader.ReadBoolean();
            if (_null)
                return;
            var indexJson = binaryReader.ReadString();
            Index = JsonConvert.DeserializeObject<Dictionary<int, Dictionary<int, List<IPoint>>>>(indexJson);
        }

        public void Write(BinaryWriter binaryWriter)
        {
            binaryWriter.Write(_null);
            if (_null)
                return;
            var indexJson = JsonConvert.SerializeObject(Index);
            binaryWriter.Write(indexJson);
        }
        #endregion
    }
}