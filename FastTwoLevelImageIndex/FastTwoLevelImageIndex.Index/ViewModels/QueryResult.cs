using System;
namespace FastTwoLevelImageIndex.Index.ViewModels
{
    public class QueryResult:IndexedImage
    {
        public Tuple<int,double> Distance { get; set; }
    }
}
