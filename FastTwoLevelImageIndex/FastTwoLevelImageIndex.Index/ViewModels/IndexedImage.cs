namespace FastTwoLevelImageIndex.Index.ViewModels
{
    public class IndexedImage
    {
        public int Id { get; set; }
        public TwoFactorDescriptor TwoFactorDescriptor { get; set; }
        public string ImageName { get; set; }
        public string Extension { get; set; }
        public string  Tag { get; set; }
        public byte[] ImageBinaryContent { get; set; }
        public string ImageDatasetName { get; set; }
    }
}
