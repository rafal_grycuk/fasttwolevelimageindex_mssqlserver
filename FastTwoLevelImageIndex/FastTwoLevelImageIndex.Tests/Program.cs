﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace FastTwoLevelImageIndex.Tests
{
    class Program
    {
        private static readonly ILog Log = LogManager.GetLogger("AllEvents");
        private static readonly ILog LogSlack = LogManager.GetLogger("Slack");

//        private static readonly string datasetName = "CorelDb";
        private static readonly string datasetName = "Pascal";

        //private static readonly string datasetName = "MicrosoftUnanotated";
        private static readonly string path = $@"{new DirectoryInfo(Environment.CurrentDirectory).Parent.Parent.Parent.Parent.FullName}\ImageDatasets\{datasetName}";
        // $@"D:\PROJECTS\Fast_Two_Level_Image_Index_on_MSSQL\ImageDatasets\{.datasetName}";

        private static readonly string[] Extensions = { ".jpg", ".png", ".bmp" };
        //private static ConcurrentBag<IImage> trainImages;
        //private static ConcurrentBag<IImage> evalImages;

        private static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            Console.WriteLine("Press any key to start inserting data.");
            Console.ReadKey();
            InsertAllImages(path, datasetName);
            Console.ReadKey();
        }
        private static void InsertAllImages(string path, string dataSetName)
        {
            try
            {
                DateTime start = DateTime.Now;
                string trainPath = $@"{path}\train";
                int totalImages = 0;
                if (Directory.Exists(trainPath))
                {
                    var folders = Directory.GetDirectories(trainPath).ToList();
                    Parallel.ForEach(folders, folder =>
                    {
                        DirectoryInfo di = new DirectoryInfo(folder);
                        var files = di.GetFiles()
                            .Where(f => Extensions.Contains(f.Extension.ToLower()))
                            .ToArray();
                        Parallel.ForEach(files, file =>
                        {
                            byte[] imageBytes = File.ReadAllBytes(file.FullName);
                            var result = InsertImage(imageBytes, file, di.Name, dataSetName);
                            if (result)
                            {
                                Log.Info(file.FullName);
                                ++totalImages;
                            }
                            else
                            {
                                Log.Error($"FileName: {file.FullName}, Tag: {di.Name}");
                            }
                        });
                    });
                    DateTime end = DateTime.Now;
                    TimeSpan execTime = end - start;
                    var successInfo = $"Inserting of {totalImages} images into {dataSetName} last {execTime}.";
                    Console.WriteLine(successInfo);
                    LogSlack.Info(successInfo);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                LogSlack.Error(ex);
                Console.WriteLine(ex.Message);
            }
        }

        private static bool InsertImage(byte[] imageBytes, FileInfo file, string tag, string dataSetName)
        {
            SqlConnection sqlConnection  = new SqlConnection(ConfigurationManager.ConnectionStrings["IndexedCbirConnectionDebug"].ConnectionString);
            var cmdStr = $"INSERT INTO dbo.Images (TwoFactorDescriptor, ImageName, Extension, Tag, ImageBinaryContent, ImageDatasetName) VALUES (CONVERT(TwoFactorDescriptor, @filePath), @fileName, @extension, @tag, @imageBinaryContent, @imageDatasetName)";
            SqlTransaction transaction = null;
            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();
                transaction = sqlConnection.BeginTransaction();
                var cmd = new SqlCommand(cmdStr)
                {
                    Connection = sqlConnection,
                    Transaction = transaction,
                    CommandTimeout = 0
                };
                cmd.Parameters.Add(new SqlParameter("@imageDatasetName", SqlDbType.VarChar));
                cmd.Parameters["@imageDatasetName"].Value = dataSetName;
                cmd.Parameters.Add(new SqlParameter("@filePath", SqlDbType.VarChar));
                cmd.Parameters["@filePath"].Value = file.FullName;
                cmd.Parameters.Add(new SqlParameter("@fileName", SqlDbType.VarChar));
                cmd.Parameters["@fileName"].Value = file.Name;
                cmd.Parameters.Add(new SqlParameter("@extension", SqlDbType.VarChar));
                cmd.Parameters["@extension"].Value = file.Extension;
                cmd.Parameters.Add(new SqlParameter("@tag", SqlDbType.VarChar));
                cmd.Parameters["@tag"].Value = tag;
                cmd.Parameters.Add(new SqlParameter("@imageBinaryContent", SqlDbType.Binary));
                cmd.Parameters["@imageBinaryContent"].Value = imageBytes;
                cmd.ExecuteNonQuery();
                transaction.Commit();
                Console.WriteLine(file.FullName);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Log.Error(ex);
                transaction?.Rollback();
                throw;
            }
            finally
            {
                if (sqlConnection.State != ConnectionState.Closed) sqlConnection.Close();
            }
        }
    }
}
