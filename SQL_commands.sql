
-- enable clr for SQL
sp_configure 'clr enabled', 1;
reconfigure with override;

-- set trustworthy on
ALTER DATABASE IndexedCbirDB SET TRUSTWORTHY ON
reconfigure with override;

select name,is_trustworthy_on from sys.databases

-- register SystemDrawing
USE [IndexedCbirDB]
CREATE ASSEMBLY [System.Drawing]
FROM 'C:\Windows\Microsoft.NET\Framework\v4.0.30319\System.Drawing.dll' 
WITH PERMISSION_SET = unsafe


-- register MathNet.Iridium
USE [IndexedCbirDB]
CREATE ASSEMBLY [MathNet.Iridium]
FROM 'C:\Windows\Microsoft.NET\Framework\v4.0.30319\MathNet.Iridium.dll' 
WITH PERMISSION_SET = unsafe

-- register [Newtonsoft.Json]
USE [IndexedCbirDB]
CREATE ASSEMBLY [Newtonsoft.Json]
FROM 'C:\Windows\Microsoft.NET\Framework\v4.0.30319\Newtonsoft.Json.dll' 
WITH PERMISSION_SET = unsafe


--list of registered assemblies
select 
    *
from sys.assembly_files f
full outer join  sys.assemblies a
    on f.assembly_id=a.assembly_id
full outer join  sys.assembly_modules m
    on a.assembly_id=m.assembly_id

--DROP ASSEMBLY [MathNet.Iridium]
--DROP ASSEMBLY [System.Drawing]


ALTER DATABASE [master]
    SET RECOVERY SIMPLE,
    TRUSTWORTHY OFF;
GO
-- add the follogin code to any publish script before CREATE ASSEMBLY....
ALTER DATABASE [master] SET TRUSTWORTHY ON
ALTER DATABASE [IndexedCbirDB] SET TRUSTWORTHY ON
