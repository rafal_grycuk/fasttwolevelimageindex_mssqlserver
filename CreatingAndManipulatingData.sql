--creating table with TwoFactorDescriptor
IF NOT EXISTS
   (  SELECT [name] 
      FROM sys.tables
      WHERE [name] = 'IndexedCbirDB'
   )
create table IndexedCbirDB.dbo.Images
(
Id int primary key identity(1,1),
TwoFactorDescriptor TwoFactorDescriptor not null,
ImageName varchar(max) not null,
Extension varchar(10) not null,
Tag	varchar(max) not null,
ImageBinaryContent varbinary(max) not null,
ImageDatasetName varchar(max) not null
);

-- Example Select 

DECLARE @filedata AS varbinary(max)
SET @filedata = (SELECT * FROM OPENROWSET(BULK N'D:\PROJECTS\Fast_Two_Level_Image_Index_on_MSSQL\ImageDatasets\Pascal\train\accordion\image_0014.png', SINGLE_BLOB) as BinaryData)
DeCLARE @json as nvarchar(max) = (select CONVERT(TwoFactorDescriptor, 'D:\PROJECTS\Fast_Two_Level_Image_Index_on_MSSQL\ImageDatasets\Pascal\train\accordion\image_0014.png').ToString())
SELECT *  
FROM OPENJSON(@json)

--- Insertation test
INSERT INTO dbo.Images (TwoFactorDescriptor, ImageName, Extension, Tag, ImageBinaryContent, ImageDatasetName) VALUES (CONVERT(TwoFactorDescriptor, 'D:\PROJECTS\Fast_Two_Level_Image_Index_on_MSSQL\ImageDatasets\Pascal\train\accordion\image_0014.png'), 'image_0014.png', '.png', 'accordion', @filedata, 'PASCAL');  
SELECT  [Id], TwoFactorDescriptor.ToString() as Descriptor,[ImageName],[Extension],[Tag],[ImageBinaryContent],[ImageDatasetName] from dbo.Images


--- QUERY CorelDb
DECLARE @filedata2 AS varbinary(max)
SET @filedata2 = (SELECT * FROM OPENROWSET(BULK N'D:\PROJECTS\Fast_Two_Level_Image_Index_on_MSSQL\ImageDatasets\CorelDb\train\art_dino\644010.jpg', SINGLE_BLOB) as BinaryData)
select *from dbo.ExecuteQuery(@filedata2,0.5 ,'CorelDb')
order by Distance

--- QUERY Pascal
DECLARE @filedata4 AS varbinary(max)
SET @filedata4 = (SELECT * FROM OPENROWSET(BULK N'D:\PROJECTS\Fast_Two_Level_Image_Index_on_MSSQL\ImageDatasets\Pascal\train\accordion\image_0014.png', SINGLE_BLOB) as BinaryData)
select * 
from dbo.ExecuteQuery(@filedata4,0.5 ,'Pascal')
order by Distance

